#!/bin/sh
# Starts persistent detached session for Kakoune, and connects client
startkak(){
    echo "Making new sesion"
    /usr/bin/kak -clear         # remove dead sessions
    /usr/bin/kak -d -s shell    # start server process
    /usr/bin/kak -c shell "$@"  # connect to server
}
/usr/bin/kak -c shell "$@" || startkak "$@"
